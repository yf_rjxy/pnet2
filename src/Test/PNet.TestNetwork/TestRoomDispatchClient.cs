﻿using System;
using System.Collections.Generic;
using PNetR;

namespace PNet.TestNetwork
{
    public class TestRoomDispatchClient : ADispatchClient
    {
        public PNetS.Room DispatchRoom;
        public TestDispatchServer Server { get; set; }

        readonly Queue<NetMessage> _messages = new Queue<NetMessage>();

        protected override void Setup()
        {
            
        }

        protected override void Connect()
        {
            UpdateConnectionStatus(ConnectionStatus.Connecting);
            Server.BeginConnecting(this);
        }

        internal void AllowConnection(Guid guid)
        {
            Connected(guid, DispatchRoom);
            UpdateConnectionStatus(ConnectionStatus.Connected);
        }

        protected override void ReadQueue()
        {
            NetMessage[] messages;
            lock (_messages)
                messages = _messages.ToArray();
            foreach (var msg in messages)
                ConsumeData(msg);
        }

        protected override void Disconnect(string reason)
        {
            Server.Internal_Disconnect(this, reason);
        }

        protected override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected override void SendMessage(NetMessage msg, ReliabilityMode mode)
        {
            var lmsg = GetMessage(msg.Data.Length);
            msg.Clone(lmsg);
            NetMessage.RecycleMessage(msg);
            Server.ReceiveMessage(this, lmsg);
        }

        internal void ReceiveMessage(NetMessage msg)
        {
            lock (_messages)
                _messages.Enqueue(msg);
        }

        internal void Disconnected(string reason)
        {
            Debug.Log("Disconnected: {0}", reason);

            Disconnected();
            UpdateConnectionStatus(ConnectionStatus.Disconnected);
        }
    }
}
