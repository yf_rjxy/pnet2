﻿using System;
using System.Collections.Generic;
using PNetR;

namespace PNet.TestNetwork
{
    public class TestRoomServer : ARoomServer
    {
        private readonly List<TestClientRoomClient> _pendingClients = new List<TestClientRoomClient>();
        private readonly List<TestClientRoomClient> _clients = new List<TestClientRoomClient>();

        readonly Queue<Tuple<TestClientRoomClient, NetMessage>> _messages = new Queue<Tuple<TestClientRoomClient, NetMessage>>();

        protected override void Setup()
        {
            
        }

        protected override void Start()
        {

        }

        protected override void ReadQueue()
        {
            Tuple<TestClientRoomClient, NetMessage>[] messages;
            lock (_messages)
            {
                messages = _messages.ToArray();
                _messages.Clear();
            }
            foreach (var msg in messages)
                ConsumeData(msg.Item1.RoomPlayer, msg.Item2);
        }

        protected override void Shutdown(string reason)
        {
            foreach (var client in _clients)
            {
                Disconnect(client, reason);
            }
        }

        private void Disconnect(TestClientRoomClient client, string reason)
        {
            _pendingClients.Remove(client);
            _clients.Remove(client);
            client.Disconnected(reason);

            RemovePlayer(client.RoomPlayer);
        }

        internal void Internal_Disconnect(TestClientRoomClient client, string reason)
        {
            Disconnect(client, reason);
        }

        protected override NetMessage GetMessage(int size)
        {
            return NetMessage.GetMessage(size);
        }

        protected override void SendToPlayers(NetMessage msg, ReliabilityMode mode)
        {
            SendToPlayers(msg, client => true);
        }

        protected override void SendToPlayers(List<Player> players, NetMessage msg, ReliabilityMode mode)
        {
            foreach(var p in players)
                SendToPlayer(GetClient(p), msg, false);
        }

        protected override void SendToPlayer(Player player, NetMessage msg, ReliabilityMode mode)
        {
            SendToPlayer(GetClient(player), msg, true);
        }

        protected override void SendExcept(NetMessage msg, Player except, ReliabilityMode mode)
        {
            SendToPlayers(msg, client => client.RoomPlayer != except);
        }

        void SendToPlayers(NetMessage msg, Predicate<TestClientRoomClient> pred)
        {
            foreach (var c in _clients)
            {
                if (pred(c))
                    SendToPlayer(c, msg, false);
            }
            NetMessage.RecycleMessage(msg);
        }

        private void SendToPlayer(TestClientRoomClient client, NetMessage msg, bool recycle)
        {
            if (client != null)
            {
                var send = NetMessage.GetMessage(msg.LengthBytes);
                msg.Clone(send);
                client.ReceiveMessage(send);
            }
            if (recycle)
                NetMessage.RecycleMessage(msg);
        }

        private TestClientRoomClient GetClient(Player player)
        {
            return player.Connection as TestClientRoomClient;
        }

        protected override void SendSceneView(NetMessage msg, ReliabilityMode mode)
        {
            SendToPlayers(msg, client => true);
        }

        protected override void AllowConnect(Player player)
        {
            GetClient(player).AllowConnect(player, Room.RoomId);
        }

        protected override void Disconnect(Player player, string reason)
        {
            Disconnect(GetClient(player), reason);
        }

        protected override void SendToConnections(List<object> connections, NetMessage msg, ReliabilityMode reliable)
        {
            foreach (var c in connections)
                SendToPlayer(c as TestClientRoomClient, msg, false);
            NetMessage.RecycleMessage(msg);
        }

        internal void Connect(TestClientRoomClient client, Guid token)
        {
            var player = ConstructNewPlayer(client);
            client.RoomPlayer = player;
            if (Room.PlayerCount > Room.Configuration.MaximumPlayers)
            {
                client.Disconnected(DtoPMsgs.NoRoom);
            }
            else
            {
                VerifyPlayerConnecting(player, token);
            }
        }

        internal void ReceiveMessage(TestClientRoomClient client, NetMessage send)
        {
            lock (_messages)
                _messages.Enqueue(new Tuple<TestClientRoomClient, NetMessage>(client, send));
        }
    }
}
